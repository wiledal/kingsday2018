!function(){
  console.log("KINGS DAY 2k18")

  var fog = new THREE.FogExp2(0xff511c, .06)

  var scene = new THREE.Scene()
  var renderer = new THREE.WebGLRenderer({
    alpha: true
  })
  renderer.setSize(window.innerWidth, window.innerHeight)
  var camera = new THREE.PerspectiveCamera(80)

  scene.fog = fog

  var composer = new POSTPROCESSING.EffectComposer(renderer)
  composer.addPass(new POSTPROCESSING.RenderPass(scene, camera))

  var pixelPass = new POSTPROCESSING.PixelationPass(10)
  pixelPass.renderToScreen = true
  composer.addPass(pixelPass)

  var clock = new THREE.Clock()

  scene.add(camera)

  var canvas = renderer.domElement

  var objects = {}

  objects.uniforms = {}
  objects.shaders = {}
  objects.textures = {}

  objects.uniforms.time = {
    value: 0
  }

  function loadCrown (callback) {
    var loader = new THREE.ColladaLoader()

    loader.load('ym.dae', function(resp) {
      objects.crown = resp.scene
      var scale = 2
      objects.crown.scale.set(scale, scale, scale)
      callback()
    })
  }

  function loadFont (callback) {
    var loader = new THREE.FontLoader()

    loader.load('upheaval.font.three.json', function (resp) {
      objects.fontUpheaval = resp
      callback()
    })
  }

  function loadShader (callback) {
    var shaders = {
      flowing: 'flowing-vertex.glsl',
      wavyPhong: 'wavy-phong-vert.glsl'
    }

    var shadersToLoad = 0
    var shadersLoaded = 0

    for (key in shaders) {
      shadersToLoad++
      loadOne(shaders[key], key)
    }

    function loadOne (url, key) {
      var xhr = new XMLHttpRequest()
      xhr.open('get', url)
      xhr.send()
      xhr.onload = function() {
        objects.shaders[key] = xhr.responseText
        shadersLoaded++

        if (shadersToLoad == shadersLoaded) {
          callback()
        }
      }
    }
  }

  function loadImage (callback) {
    var images = {
      willem: 'willem.jpg',
      checkers: 'checkers2.jpg'
    }

    var imagesToLoad = 0
    var imagesLoaded = 0

    function loadOne (key, url) {
      var loader = new THREE.TextureLoader()
      loader.load(url, function(resp) {
        objects.textures[key] = resp
        imagesLoaded++

        if (imagesLoaded == imagesToLoad) {
          callback()
        }
      })
    }

    for (var key in images) {
      imagesToLoad++

      loadOne(key, images[key])
    }
  }

  function loadAll (callback) {
    var toLoad = 4

    function assetLoaded () {
      toLoad--
      if (toLoad <= 0) {
        if (bowser.mobile || bowser.tablet) {
          //alert('i am device')
          return waitForTap()
        }
        startApp()
      }
    }

    loadCrown(assetLoaded)
    loadFont(assetLoaded)
    loadShader(assetLoaded)
    loadImage(assetLoaded)

  }

  function animateCanvasIn () {
    document.body.appendChild(canvas)

    canvas.style.opacity = 0

    canvas.offsetWidth
    canvas.style.transition = 'all 1s linear'
    canvas.style.opacity = 1
  }

  function addLights () {
    objects.lights = [
      new THREE.PointLight(),
      new THREE.PointLight(),
      new THREE.PointLight()
    ]

    objects.lights[0].position.set(10, 10, 0)
    objects.lights[0].position.set(-10, 10, -10)
    objects.lights[0].position.set(8, 2, 10)

    objects.lights.forEach(function(l) {
      scene.add(l)
    })
  }

  function makeText () {
    var geometry = new THREE.TextGeometry('Your Majesty presents: —Keygen to the Kingdom— Wishing you all a happy Kingsday! ps scrollers sucks.', {
    		font: objects.fontUpheaval,
    		size: .8,
    		height: .2,
    		curveSegments: 5,
    		// bevelEnabled: true,
    		// bevelThickness: .1,
    		// bevelSize: .1,
    		// bevelSegments: 10
    	});

      var mesh = new THREE.Mesh(
        geometry,
        // new THREE.MeshLambertMaterial({
        //   color: 0xff0000
        // })
        new THREE.ShaderMaterial({
          uniforms: Object.assign({}, THREE.ShaderLib.standard.uniforms, {
           diffuse: { value: new THREE.Color(0x5d5fff) },
           metalness: { value: .2 },
           roughness: { value: .6 },
           uTime: objects.uniforms.time,
           uAmount: { value: 1},
           uRandom: { value:  1 }
         }),
         lights: true,
         defines: {
           USE_MAP: false,
         },
         wireframe: false,
         fragmentShader: THREE.ShaderChunk.meshphysical_frag,
         vertexShader: objects.shaders.flowing
       })
      )

    objects.text = mesh

    console.log(objects.text)
    scene.add(mesh)
  }

  function makeImage () {
    var aspect = objects.textures.willem.image.width /  objects.textures.willem.image.height

    function makeWillem (scale) {
      return new THREE.Mesh(
        new THREE.PlaneGeometry(aspect * scale,scale,10,10),
        new THREE.MeshBasicMaterial({
          map: objects.textures.willem
        })
      )
    }

    objects.willems = [
      makeWillem(3),
      makeWillem(1),
      makeWillem(1),
      makeWillem(1),
      makeWillem(1)
    ]

    objects.willems.forEach(function(w) {
      //scene.add(w)
    })
  }

  function waitForTap () {
    document.querySelector('.preloader span').innerHTML = 'TAP FOR CANDY 🍭'

    document.querySelector('.preloader').onclick = startApp
  }

  function makeFloor () {
    var texture = objects.textures.checkers
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set( 10, 10 );
    //texture.needsUpdate = true
    var m = new THREE.Matrix3()
    m.set(128, 0, 0, 0, 80, 0, 0, 0, 0)

    var mesh = new THREE.Mesh(
      new THREE.PlaneGeometry(200, 200, 50, 50),
       new THREE.ShaderMaterial({
         uniforms: Object.assign({}, THREE.ShaderLib.phong.uniforms, {
          diffuse: { value: new THREE.Color(0xffffff) },
          shininess: { value: 0 },
          uTime: objects.uniforms.time,
          uvTransform: { value: m },
          map: {
            value: texture
          }
        }),
        defines: {
          USE_MAP: true
        },
        lights: true,
        fog: true,
        fragmentShader: THREE.ShaderChunk.meshphong_frag,
        vertexShader: objects.shaders.wavyPhong
      })
    )

    mesh.rotation.x = -Math.PI / 2
    mesh.position.y = -5
    //mesh.position.z = -15

    var ceiling = mesh.clone()

    ceiling.rotation.x = Math.PI / 2
    ceiling.position.y = 7

    objects.floor = mesh
    objects.ceiling = ceiling

    scene.add(ceiling)
    scene.add(mesh)
  }

  function startApp () {
    if (document.body.webkitRequestFullscreen) document.body.webkitRequestFullscreen();
    document.querySelector('.preloader').parentNode.removeChild(document.querySelector('.preloader'))
    document.querySelector('audio').play()
    animateCanvasIn()

    addLights()
    makeText()
    makeFloor()

    makeImage()

    scene.add(objects.crown)

    render()
  }

  function resize () {
    renderer.setSize(window.innerWidth, window.innerHeight)
    pixelPass.setSize(window.innerWidth, window.innerHeight)
    pixelPass.granularity = window.innerHeight / 1200 * 8
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
  }

  var tick = 0
  function render () {
    requestAnimationFrame(render)
    var now = Date.now()
    tick++

    objects.uniforms.time.value = tick

    camera.position.z = 8 + Math.sin(now/800) * .5
    camera.position.x = Math.sin(now/1200) * .5
    camera.position.y = Math.sin(now/580) * .4

    //camera.lookAt(objects.crown.position)

    objects.crown.rotation.z = Math.sin(now / 508)
    objects.crown.rotation.y = Math.cos(now / 530) * .06
    objects.crown.rotation.x = -Math.PI / 2 + Math.sin(now / 600) * .4 + Math.cos(now / 1209) * .1
    objects.crown.position.z = -4 + Math.sin(now / 882) * 2

    objects.willems.forEach(function(w, i) {
      w.position.x = Math.sin(now / 600 + i * 200) * 4
      w.position.z = Math.cos(now / 600+ i * 200) * 4
      w.position.y = Math.cos(now / 800+ i * 200) * 2

      w.rotation.x = Math.sin(now / 400+ i * 200) * .4
      w.rotation.z = Math.sin(now / 520+ i * 200) * .4
      w.rotation.y = Math.sin(now / 726+ i * 200) * .4
    })

    objects.text.position.x = 18 + (tick % 2000)/2000 * -140
    objects.text.position.z = 1

    objects.floor.rotation.z += 0.003
    objects.ceiling.rotation.z += 0.003

    composer.render(clock.getDelta())
  }

  window.addEventListener('resize', resize)

  console.log(scene)

  resize()
  loadAll()
}()
